#!/bin/bash

GREEN='\033[0;32m'
YELLOW='\033[0;33m'
RED='\033[0;31m'
NC='\033[0m'

drush() {
  docker-compose exec --user 33 app /var/www/html/vendor/bin/drush --root="/var/www/html/web" $@
}

drupal() {
  docker-compose exec --user 33 app /var/www/html/vendor/bin/drupal $@
}

composer() {
  docker-compose exec --user 33 app composer "$@"
}

behat() {
  docker-compose exec app vendor/bin/behat --config web/profiles/contrib/gu_profile/behat.yml $@
}

phpunit() {
  docker-compose exec --user 33 app vendor/bin/phpunit -c web/core $@
}

phpcs() {
  docker-compose exec --user 33 app vendor/bin/phpcs --runtime-set ignore_warnings_on_exit 1 --standard=vendor/drupal/coder/coder_sniffer/Drupal/ruleset.xml --ignore=*.md,*.css,*.js,*.txt,features/bootstrap,node_modules $@
}

phpcbf() {
  docker-compose exec --user 33 app vendor/bin/phpcbf --standard=vendor/drupal/coder/coder_sniffer/Drupal/ruleset.xml $@
}

devgulp() {
  # Symlink themes in portal repository into gu_profile
  ln -fsr src/web/themes/custom/* src/web/profiles/contrib/gu_profile/themes/gu_themes/
  echo "Building themes..."
  (cd src/web/profiles/contrib/gu_profile/themes/gu_themes && yarn install && npx gulp build)
  (cd src/web/profiles/contrib/gu_profile/themes/gu_admin && yarn install && npx gulp build)
  # Copy CSS for React local development environment
  (cp src/web/profiles/contrib/gu_profile/themes/gu_themes/gu_base/dist/css/gu_base.css src/web/libraries/web-react-search/public/)
}

# Use case: updating composer.lock in portal image without first building a dev-image.
install_composer() {
  docker-compose exec -u root app bash -c \
    "curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer"

  # Copy ssh keys (copy is not supported by docker-compose, hence the work-around):
  docker-compose exec app bash -c "mkdir /var/www/.ssh"
  cat docker/ssh/bitbucket_access_key | docker-compose exec -T app bash -c "cat > /var/www/.ssh/id_rsa"
  cat docker/ssh/bitbucket_access_key.pub | docker-compose exec -T app bash -c "cat > /var/www/.ssh/id_rsa.pub"
  docker-compose exec app bash -c "ssh-keyscan -H bitbucket.org > /var/www/.ssh/known_hosts"
  docker-compose exec app bash -c "chown www-data:www-data /var/www/.ssh/*"
  docker-compose exec app bash -c "chmod 400 /var/www/.ssh/id_rsa"
}

# Install oc, tkn, helm and helmfile (linux-amd64).
install_devtools() {
  set -e
  wget -c https://downloads-openshift-console.apps.k8s.gu.se/amd64/linux/oc.tar -O - -q | tar -x -C /usr/local/bin
  wget -c https://mirror.openshift.com/pub/openshift-v4/clients/pipeline/latest/tkn-linux-amd64-0.17.2.tar.gz -O - -q | tar -xz -C /usr/local/bin
  wget -c https://mirror.openshift.com/pub/openshift-v4/clients/helm/latest/helm-linux-amd64.tar.gz -O - -q | tar -xz -C /usr/local/bin --transform 's/helm-linux-amd64/helm/'
  wget -c https://github.com/roboll/helmfile/releases/latest/download/helmfile_linux_amd64 -O /usr/local/bin/helmfile -N -q
  (cd /usr/local/bin && chmod +x oc tkn helm helmfile)
  echo "Devtools installed successfully!"
}

drupal_site_install() {
  set -e

  # Fix permissions due to native nfs sync.
  docker-compose exec --user 33 app chmod -R 777 /var/www/html/web/sites/default/files
  time docker-compose exec --user 33 app bash -c "/var/www/html/scripts/dev.sh drupal_site_install"
}

enable_xdebug() {
  docker-compose exec -u root app bash -c "docker-php-ext-enable xdebug"
  docker-compose restart app
  echo -e "Xdebug is now ${GREEN}enabled${NC}"
}

enable_features() {
  set -e
  # Q: modules and configuration layers, multiple options:
  #    1. let modules install layer configuration (similar to config splits)
  #    2. let layers install modules
  #    3. enable layers and modules separately (current approach)
  #    Probably (1) or (2) will be a better option. (1) would be most consistent with previous approach.
  drush en -y gu_news gu_event gu_information_page gu_landing_page gu_rich_page gu_system_page gu_spaces_contact_page

  # enable feature layers
  drush clen gu_feature_news
  drush clen gu_feature_event
  drush clen gu_feature_information_page
  drush clen gu_feature_landing_page
  drush clen gu_feature_rich_page
  drush clen gu_feature_systempage
  drush clen gu_feature_spaces_contact_page

  # import enabled layers + changes in active configuration into layers; synchronize back into active configuration.
  drush clim --synchronize -y --quiet

  # N: this will be stored as a configuration override in config/layers/portal
  # docker-compose exec app bash -c 'cat /var/www/html/config/custom/custom.internal_blurb.link.allowed_bundles.yml | /var/www/html/vendor/bin/drush --root=/var/www/html/web --yes cset --input-format=yaml field.field.blurb.internal.field_linked_node settings.handler_settings.target_bundles -'
}

enable_spaces() {
  set -e
  drush en -y gu_spaces gu_spaces_menu gu_spaces_media gu_spaces_front_page gu_spaces_contact_page gu_pop
  drush clen gu_feature_spaces
  drush clen gu_feature_spaces_media
  drush clen gu_feature_spaces_front_page
  drush clen gu_feature_spaces_contact_page
  drush clen gu_feature_pop
  drush clim --synchronize -y --quiet
  drush gu:spaces:init
}

enable_pop() {
  set -e
  drush en -y gu_pop
  drush clen gu_feature_pop
  drush clim --synchronize -y --quiet
}

enable_dev() {
  set -e
  drush clen dev
  drush clim --synchronize -y --quiet
}

create_demo_content() {
  set -e
  drush gu:create-demo-content --source="../demo-content/hotel"
  drush gu:news:menus
  drush gu:event:menus
}

build_arm64(){
  DOCKER_BIN="docker"
  DOCKER_REPO=git@bitbucket.org:gu-utv/wk-docker-images.git
  DOCKER_FILES_DIR=docker/wk-docker-images
  DOCKER_BASE_DIR=docker/wk-docker-images/base/
  APP_TAG=nexus.gu.gu.se:8445/web-app-dev:latest

  # Pull latest dockerfiles.
  git -C "${DOCKER_FILES_DIR}" pull --ff-only 2>/dev/null || git clone ${DOCKER_REPO} "${DOCKER_FILES_DIR}"

  # Disable MailHog and New Relic.
  # TODO: Add support for multiarch see ticket WK-5178 and WK-5179 or line 58 and 65 in app_base.dockerfile).
  sed -e 's/^RUN go get github.com\/mailhog/#&/' \
  -e 's/^RUN cp \/root\/go\/bin\/mhsendmail/#&/' \
  -e 's/^     \/tmp\/newrelic-php5/#&/' \
  ${DOCKER_BASE_DIR}app/app_base.dockerfile > ${DOCKER_BASE_DIR}app/dockerfile
  echo -e "\n✅ ${GREEN}Disabling MailHog and New Relic!${NC}\n"

  # Extract parts from app_dev.dockerfile to bypass nexus and append in dockerfile.
  sed -n '/^USER /,/^USER /p' ${DOCKER_BASE_DIR}app/app_dev.dockerfile > ${DOCKER_BASE_DIR}app/dockerfile~
  sed -i~.bak '/USER / r '${DOCKER_BASE_DIR}'app/dockerfile~' ${DOCKER_BASE_DIR}app/dockerfile

  # Remove temp files.
  rm ${DOCKER_BASE_DIR}app/dockerfile~*

  # Change Redis image to ARM64.
  echo -e "✅ ${GREEN}Changing Redis to ARM64 image!${NC}\n"
  sed -e 's/nexus.gu.gu.se:8445\/web-redis/arm64v8\/redis/g' docker-compose.yml > docker-compose.bak
  mv docker-compose.bak docker-compose.yml

  # Rebuild app.
  echo ${DOCKER_BIN} build --no-cache --tag="${APP_TAG}" -f ${DOCKER_BASE_DIR}app/dockerfile ${DOCKER_BASE_DIR}app | bash
  echo -e "\n✅ ${GREEN}App image rebuilt!${NC}\n"

  # Restart containers.
  docker-compose restart redis || true

  # Recreate containers.
  docker-compose up -d --force-recreate app || true
}

enable_xdebug() {
  docker-compose exec -u root app bash -c "docker-php-ext-enable xdebug"
  docker-compose restart app
  echo -e "Xdebug is now ${GREEN}enabled${NC}"
}

disable_xdebug() {
  docker-compose exec -u root app bash -c "rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"
  docker-compose restart app
  echo -e "Xdebug is now ${GREEN}disabled${NC}"
}

install_xdebug() {
  set -e
  docker-compose exec -u root app bash -c "pecl install xdebug-3.0.4"
  docker-compose exec -u root app bash -c "docker-php-ext-enable xdebug"
  docker-compose exec -u root app bash -c "rm -rf /tmp/pear"

  cat << EOF | docker-compose exec -u root -T app bash -c "cat > /usr/local/etc/php/conf.d/xdebug.ini"
xdebug.client_host=host.docker.internal
xdebug.mode = debug
xdebug.start_with_request = yes
xdebug.remote_port=9003
xdebug.remote_enable=1
xdebug.max_nesting_level=500
EOF
  echo -e "Xdebug is now ${GREEN}installed${NC}"
}

opcache_reset() {
  echo "<?php opcache_reset();" > src/web/opcache_reset.php
  curl -q localhost/opcache_reset.php
}

cr() {
  opcache_reset
  drush cr
}


update_tools() {
    git archive --remote=git@bitbucket.org:josef_gu/dev.git HEAD dev.sh | tar -x
}

"$@"